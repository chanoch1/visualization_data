import matplotlib.pyplot as plt

input_values = [1, 2, 3, 4, 5]# אורך ציר ה X
squares = [1, 4, 9, 16, 25]
plt.plot(input_values, squares, linewidth=5)# made thickness 5
plt.title ("Square Numbers", fontsize=24)# כותרת
plt.xlabel("Value", fontsize=14)
plt.ylabel("Square of Value", fontsize=14)
plt.tick_params(axis='both', labelsize=14)# גודל מספרי הצירים
plt.show()
