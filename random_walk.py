from random import choice

class RandomWalk():

    def __init__(self, num_points=50000):
        self.num_points = num_points
        self.x_values = [0]
        self.y_values = [0]

    def fill_walk(self):
        self.x_values = self.get_step()
        self.y_values = self.get_step()

    def get_step(self):
        self.values = [0]
        while len(self.values) < self.num_points:
            direction = choice([1, -1])
            distance = choice([0, 1, 2, 3, 4])
            step = direction * distance
            if step == 0 and step == 0:
                continue
            next = self.values[-1] + step
            self.values.append(next)
        return self.values


